#!/usr/bin/env bash

BASEDIR=$(dirname "$0")

cd ${BASEDIR}/../botkit
git apply ../botkit_msgio_patcher/patch_slack_endpoints.patch
