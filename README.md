# Message.io patcher for howdyai/botkit #

This package substitutes all hardcoded **slack.com** API endpoints with **process.env.api_host** in [Howdy Botkit](https://github.com/howdyai/botkit).

The patch can help you to call test API endpoints or various proxy services instead of real Slack API.


### Installation ###

# Install Howdy botkit itself in your bot (if it's not installed yet)


```
npm install --save botkit
```

# Install Message.io patcher

```
npm install --save botkit_msgio_patcher
```

# Run the patching shell script

```
bash ./node_modules/botkit_msgio_patcher/run.sh
```


After that you'll find all hardcoded Slack endpoints substituted with configurable variables.